﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using Microsoft.Owin.Cors;
using Microsoft.AspNet.SignalR;
using RabbitMQ.Client;
using SignalR.RabbitMQ;

[assembly: OwinStartup(typeof(SignalRTestServer.Startup))]

namespace SignalRTestServer
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=316888
            //Allow remote cross calls
            app.UseCors(CorsOptions.AllowAll);
            app.MapSignalR();
          

            
        }
    }
    public class MyHub : Hub
    {
        public void Send(string name, string message)
        {
            Clients.All.addMessage(name, message);
        }
    }
}
