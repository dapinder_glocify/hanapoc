﻿using SignalRTestServer.Domain;
using System.Collections.Generic;

namespace SignalRTestServer.DBManager
{
    interface ICustomerManager
    {       
       
        Customer Add(Customer customer);
        Customer Update(Customer customer);
        void Remove(int id);
    }
}
