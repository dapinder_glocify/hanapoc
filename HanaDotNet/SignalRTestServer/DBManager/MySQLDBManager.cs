﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using SignalRTestServer.Domain;
using System.Configuration;
using SignalRTestServer.DBManager;
using System;

namespace SignalRTestServer.DBManager
{
    /// <summary>
    /// Custom DB class for each Database provider..This is for MySQL
    /// </summary>
    public  class MySQLDBManager : ICustomerManager
    {
        private IDbConnection _db;
        public MySQLDBManager()
        {
            _db =  new SqlConnection(ConfigurationManager.ConnectionStrings["MySqlServerConnString"].ConnectionString);
        }

        public Customer Add(Customer customer)
        {
            try
            {
                var sqlQuery = "INSERT INTO Customers (CustomerId, Name,Address,Location,dateOfBirth,IsActive) VALUES(@CustomerId, @Name,@Address,@Location,@dateOfBirth,@IsActive); ";
                var customerId = this._db.Query<int>(sqlQuery, customer).Single();
                customer.CustomerId = customerId;
                return customer;
            }
            catch(Exception ex)
            {
                return null;
            }
        }
        
        public void Remove(int id)
        {
            var sqlQuery = ("Delete From Customers Where Id = " + id + "");
            this._db.Execute(sqlQuery);
        }

        public Customer Update(Customer customer)
        {
            var sqlQuery =
            "UPDATE Customers " +
            "SET Name = @Name, " +
            " Address = @Address, " +
            " Location = @Location, " +

            "WHERE EmpID = @EmpID";
            this._db.Execute(sqlQuery, customer);
            return customer;
        }
    }
}
