﻿using Microsoft.AspNet.SignalR;
using Microsoft.Owin.Hosting;
using RabbitMQ.Client;
using SignalR.RabbitMQ;
using System;

namespace SignalRTestServer
{
    class Program
    {
        static void Main(string[] args)
        {
            // This will *ONLY* bind to localhost, if you want to bind to all addresses
            // use http://*:8080 to bind to all addresses. 
            // See http://msdn.microsoft.com/en-us/library/system.net.httplistener.aspx 
            // for more information.
            string url = "http://localhost:8088";
            using (WebApp.Start(url))
            {
               
                Console.WriteLine("Server running on {0}", url);
                Console.ReadLine();
            }
          
        }
    }
}
