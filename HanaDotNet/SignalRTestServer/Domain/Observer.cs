﻿using SignalRTestServer.DBManager;
using System;
using System.Threading.Tasks;

namespace SignalRTestServer.Domain
{
    /// <summary>
    /// Used Asynchronous way of populating multiple DB's through Task Parellel Style
    /// </summary>
    public class Observer : IObserver
    {
        ICustomerManager customerSQLServerManager = new SQLServerDBManager();
        ICustomerManager customerMySQLServerManager = new MySQLDBManager();

        public string ObserverName { get; private set; }
        public Observer(string name)
        {
            this.ObserverName = name;
        }
        public  void Update(Customer customer)
        {
            Console.WriteLine("{0}: A updated customer has arrived ",this.ObserverName);
            Task T1 = Task.Factory.StartNew(() =>
             {
                 customerSQLServerManager.Update(customer);
             });
            Task T2 = Task.Factory.StartNew(() =>
            {
                customerMySQLServerManager.Update(customer);
            });
            Task.WaitAll(T1, T2);
            
        }

        public void Insert(Customer customer)
        {
            Console.WriteLine("{0}: A New customer has arrived ", this.ObserverName);
            Task T1 = Task.Factory.StartNew(() =>
            {
                customerSQLServerManager.Add(customer);
            });
         
            Task T2 = Task.Factory.StartNew(() =>
            {
                customerMySQLServerManager.Add(customer);
            });
          
            Task.WaitAll(T1, T2);
           
           
        }

        public void Remove(Customer customer)
        {
            Console.WriteLine("{0}: Lets remove this customer ", this.ObserverName);
            Task T1 = Task.Factory.StartNew(() =>
            {
                customerSQLServerManager.Remove(customer.CustomerId);
            });
            Task T2 = Task.Factory.StartNew(() =>
            {
                customerMySQLServerManager.Add(customer);
            });
            Task.WaitAll(T1, T2);
            
          
        }
    }

  
}
