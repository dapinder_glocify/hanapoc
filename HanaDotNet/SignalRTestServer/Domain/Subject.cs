﻿using System.Collections.Generic;

namespace SignalRTestServer.Domain
{
    public class Subject : ISubject
    {
        private List<Observer> observers = new List<Observer>();

        private  Customer customer = new Customer();
        public void NotifyAll(Customer customer,string operation)
        {
            this.customer = customer;
            Notify(operation);
        }
       
        public void Subscribe(Observer observer)
        {
            observers.Add(observer);
        }

        public void Unsubscribe(Observer observer)
        {
            observers.Remove(observer);
        }

        public void Notify(string operation)
        {
            if (string.Equals(operation, "Add"))
            {
                observers.ForEach(x => x.Insert(customer));
            }
            else if(string.Equals(operation, "Update"))
            {
                observers.ForEach(x => x.Update(customer));
            }
            else if(string.Equals(operation, "Remove"))
            {
                observers.ForEach(x => x.Remove(customer));
            }
            else
            {

            }
        }
    }

}
