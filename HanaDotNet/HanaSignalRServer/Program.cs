﻿
using Microsoft.AspNet.SignalR;
using Microsoft.Owin.Hosting;
using System;

namespace HanaSignalRServer
{
    class Program
    {
        static void Main(string[] args)
        {
            //Replace this with your Hosting URL
            string url = "http://localhost:8080";
            using (WebApp.Start(url))
            {

                Console.WriteLine("Server running on {0}", url);
                Console.ReadLine();

            }
        }
    }
}
