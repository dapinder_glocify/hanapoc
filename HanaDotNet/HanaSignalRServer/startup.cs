﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using Microsoft.Owin.Cors;
using Microsoft.AspNet.SignalR;

[assembly: OwinStartup(typeof(HanaSignalRServer.Startup))]
namespace HanaSignalRServer
{
    class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            //app.UseCors(CorsOptions.AllowAll);
            //app.MapSignalR();
            app.Map("/signalr", map =>
            {

                app.UseCors(CorsOptions.AllowAll);
                app.MapSignalR();
            });
        }
    }
}
