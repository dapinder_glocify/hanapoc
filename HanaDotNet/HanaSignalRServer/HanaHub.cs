﻿using HanaSignalRServer.Domain;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Threading.Tasks;

namespace HanaSignalRServer
{
    //[HubName("hanaHub")]
    class HanaHub : Hub
    {
        Subject subject = new Subject();
        public void Send(string name, string message)
        {
            Clients.All.addMessage(name, message);
        }

        public override Task OnConnected()
        {
            //Initialize SAP Hana DBConnector Or SAP Streaming Data Rest API Invocation
            Console.WriteLine("Hub OnConnected {0}\n", Context.ConnectionId);

            return (base.OnConnected());
        }
        public async Task<Customer> GetData(Customer customer)
        {

            //Fetch data through SAP Hana Connector and store inside an Object
          return  await Clients.All.SelectCustomers(customer);
        }
        public async Task<Customer> InsertCustomers(Customer customer)
        {
            //Fetch data through SAP Hana Connector and store inside an Object
            Observer observer1 = new Observer("Data Insertion..");
            subject.Subscribe(observer1);
            subject.NotifyAll(customer,"Add");
            return await Clients.All.AddCustomers(customer);
        }
        public async Task<Customer> UpdateData(Customer customer)
        {
            //Fetch data through SAP Hana Connector and store inside an Object
            Observer observer1 = new Observer("Data Updation..");
            subject.Subscribe(observer1);
            subject.NotifyAll(customer,"Update");
            return await Clients.All.UpdateCustomers(customer);
        }
        public async Task<Customer> RemoveData(Customer customer)
        {
            //Fetch data through SAP Hana Connector and store inside an Object
            Observer observer1 = new Observer("Data Deletion..");
            subject.Subscribe(observer1);
            subject.NotifyAll(customer,"Remove");
            return await Clients.All.RemoveCustomers(customer);
        }
    }
}
