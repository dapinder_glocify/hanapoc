﻿using HanaSignalRServer.Domain;
using System.Collections.Generic;

namespace HanaSignalRServer.DBManager
{
    interface ICustomerManager
    {       
       
        Customer Add(Customer customer);
        Customer Update(Customer customer);
        void Remove(int id);
    }
}
