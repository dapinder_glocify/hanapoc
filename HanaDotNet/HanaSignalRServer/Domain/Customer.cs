﻿using System;

namespace HanaSignalRServer.Domain
{
    public class Customer
    {
       public int CustomerId { get; set; }
       public string Name { get; set; }  
       public string Address { get; set; }
       public string Location { get; set; }
       public DateTime? DateOfBirth { get; set; }
       public bool isActive { get; set; }

    }
}
