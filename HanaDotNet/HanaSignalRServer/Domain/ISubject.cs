﻿namespace HanaSignalRServer.Domain
{
    interface ISubject
    {
        void Subscribe(Observer observer);
        void Unsubscribe(Observer observer);
        void Notify(string operation);
    }
}
