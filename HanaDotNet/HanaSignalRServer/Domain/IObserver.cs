﻿namespace HanaSignalRServer.Domain
{
    interface IObserver
    {
        //This method will update 3rd Part DB's based on Notifications received from Hana
        void Insert(Customer customer);
        void Remove(Customer customer);
        void Update(Customer customer);
    }
}
