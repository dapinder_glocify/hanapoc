﻿using CommonLib;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Client;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace HanaSignalRClient
{
    class Program
    {
        private static ConnectionFactory _factory;
        private static RabbitMQ.Client.IConnection _connection;
        private static IModel _model;


        private const string QueueName = "StandardQueue_DataFromHadoop";
        static void Main(string[] args)
        {
            var connection = new HubConnection("http://localhost:8080/");
            //Make proxy to hub based on hub name on server
            var myHub = connection.CreateHubProxy("MyHub");
             connection.Start().Wait();
            connection.Start().ContinueWith(task => {
                if (task.IsFaulted)
                {
                    Console.WriteLine("There was an error opening the connection:{0}",
                                      task.Exception.GetBaseException());
                }
                else
                {
                    Console.WriteLine("Connected");
                }

            }).Wait();



            connection.Stop();
            Console.ReadKey();

         
            Recieve();
        }

        
        public static void Recieve()
        {

            _factory = new ConnectionFactory
            {
                HostName = "localhost",
                UserName =
          "guest",
                Password = "guest"
            };
            _connection = _factory.CreateConnection();
            _model = _connection.CreateModel();
            _model.QueueDeclare(QueueName, true, false, false, null);


            var consumer = new  QueueingBasicConsumer(_model);

            var msgCount = GetMessageCount(_model, QueueName);
            _model.BasicConsume(QueueName, true, consumer);

            var count = 0;

            while (count < msgCount)
            {
                var message = (Customer)consumer.Queue.Dequeue().Body.DeSerialize();

                Console.WriteLine("----- Received {0} : {1}", message.CustomerId,
message.Name);
                count++;
            }
        }
        private static uint GetMessageCount(IModel channel, string queueName)
        {
            var results = channel.QueueDeclare(queueName, true, false, false, null);
            return results.MessageCount;
        }
        private static Customer DeserialiseFromBinary(byte[] messageBody)
        {
            MemoryStream memoryStream = new MemoryStream();
            memoryStream.Write(messageBody, 0, messageBody.Length);
            memoryStream.Seek(0, SeekOrigin.Begin);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            return binaryFormatter.Deserialize(memoryStream) as Customer;
        }
    }
}