﻿using CommonLib;
using Microsoft.AspNet.SignalR.Client;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HanaHadoopLinQDataFetch
{
    //https://dzone.com/articles/hdinsight-writing-hadoop-map

        //This fetches data from Hadoop or HanaDB and pushed to RabbitMQ Queue
    class Program
    {
        private static ConnectionFactory _factory;
        private static RabbitMQ.Client.IConnection _connection;
        private static IModel _model;

        private const string QueueName = "StandardQueue_DataFromHadoop";

        static void Main(string[] args)
        {

            var connection = new HubConnection("http://localhost:8088/");
            //Make proxy to hub based on hub name on server
            var myHub = connection.CreateHubProxy("HanaHub");
            connection.Start().Wait();
            connection.Start().ContinueWith(task => {
                if (task.IsFaulted)
                {
                    Console.WriteLine("There was an error opening the connection:{0}",
                                      task.Exception.GetBaseException());
                }
                else
                {
                    Console.WriteLine("Connected");
                }

            }).Wait();

            myHub.Invoke("Send", "client message", " sent from console client").ContinueWith(task =>
            {
                if (task.IsFaulted)
                {
                    Console.WriteLine("!!! There was an error opening the connection:{0} \n", task.Exception.GetBaseException());
                }

            }).Wait();
            Console.WriteLine("Client Sending addMessage to server\n");


            //Lets work with Single Recod and pushed to SignalR
            //For Insertion
            Customer cust1 = new Customer() { Name = "Customer3", Address = "Near Western Park 3", CustomerId = 103, DateOfBirth = new System.DateTime(1977, 4, 2).ToString(), isActive = true, Location = "Punjab" };
            myHub.Invoke<Customer>("InsertCustomers", cust1).ContinueWith(task =>
            {
                if (task.IsFaulted)
                {
                    Console.WriteLine("!!! There was an error opening the connection:{0} \n", task.Exception.GetBaseException());
                }

            }).Wait();
            Console.WriteLine("Client Sending addMessage to server\n");
            //

            // RabbitMQFunc();
            connection.Stop();
            Console.ReadKey();
        }

        private static void RabbitMQFunc()
        {
            //  Writing Hadoop Map Reduce Jobs In C# And Querying Results Back Using LINQ 
            //Mocking data
            IList<Customer> customerList = new List<Customer>();
            customerList.Add(new Customer() { Name = "Customer1", Address = "Near Western Park 1", CustomerId = 101, DateOfBirth = new System.DateTime(1977, 4, 2).ToString(), isActive = true, Location = "Punjab" });
            customerList.Add(new Customer() { Name = "Customer2", Address = "Near Western Park 2", CustomerId = 101, DateOfBirth = new System.DateTime(1967, 1, 8).ToString(), isActive = true, Location = "Haryana" });
            customerList.Add(new Customer() { Name = "Customer3", Address = "Near Western Park 3", CustomerId = 101, DateOfBirth = new System.DateTime(1972, 6, 13).ToString(), isActive = true, Location = "Delhi" });
            customerList.Add(new Customer() { Name = "Customer4", Address = "Near Western Park 4", CustomerId = 101, DateOfBirth = new System.DateTime(1981, 9, 29).ToString(), isActive = true, Location = "Himachal" });
            customerList.Add(new Customer() { Name = "Customer5", Address = "Near Western Park 5", CustomerId = 101, DateOfBirth = new System.DateTime(1985, 11, 1).ToString(), isActive = true, Location = "J&K" });

            CreateQueue();
            foreach (var rec in customerList)
            {
                SendDataToQueue(rec);
            }
            Recieve();
        }




        /// <summary>
        /// Creates rabbitMQ standard Queue
        /// </summary>
        private static void CreateQueue()
        {
            _factory = new ConnectionFactory
            {
                HostName = "localhost",
                UserName = "guest",
                Password = "guest"
            };
            _connection = _factory.CreateConnection();
            _model = _connection.CreateModel();
            _model.QueueDeclare(QueueName, true, false, false, null);

        }
        private static void SendDataToQueue(Customer message)
        {
            _model.BasicPublish("", QueueName, null, message.Serialize());
            Console.WriteLine(" [x] MQ Message Sent : {0} : {1}",
message.CustomerId, message.Name);
        }
        public static void Recieve()
        {

            _factory = new ConnectionFactory
            {
                HostName = "localhost",
                UserName =
          "guest",
                Password = "guest"
            };
            _connection = _factory.CreateConnection();
            _model = _connection.CreateModel();
            _model.QueueDeclare(QueueName, true, false, false, null);


            var consumer = new QueueingBasicConsumer(_model);

            var msgCount = GetMessageCount(_model, QueueName);
            _model.BasicConsume(QueueName, true, consumer);

            var count = 0;

            while (count < msgCount)
            {
                var message = (Customer)consumer.Queue.Dequeue().Body.DeSerialize();

                Console.WriteLine("----- Received {0} : {1}", message.CustomerId,
message.Name);
                count++;
            }
        }
        private static uint GetMessageCount(IModel channel, string queueName)
        {
            var results = channel.QueueDeclare(queueName, true, false, false, null);
            return results.MessageCount;
        }
    }
}
