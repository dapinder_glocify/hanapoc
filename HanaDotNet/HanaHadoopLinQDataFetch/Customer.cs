﻿using System;

namespace HanaHadoopLinQDataFetch
{
    [Serializable]
    public class Customer
    {
        public int CustomerId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Location { get; set; }
        public string DateOfBirth { get; set; }
        public bool isActive { get; set; }

    }
}
